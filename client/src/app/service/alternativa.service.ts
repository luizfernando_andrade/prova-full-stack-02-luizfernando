import { Alternativa } from './../../model/alternativa';
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Estudante } from "../../model/estudante";
import { Subscriber } from "rxjs/Subscriber";

@Injectable()
export class AlternativaService {

    baseDir: string = "http://localhost:1337/parse";
    estudantes: Estudante[] = [];
    constructor(private http: Http) { }

    carregarAlternativas() : Observable<Response> {
        return this.http.post(this.baseDir + "/functions/carregar-alternativas", {});
    }

    salvarAlternativa(alternativa: Alternativa): Observable<Response> {
        return this.http.post(this.baseDir + "/classes/Alternativa", alternativa);
    }

    removerAlternativa(alternativa: Alternativa): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/deleta-alternativa?objectId=" + alternativa.objectId,{});
    }

    editarAlternativa(alternativa: Alternativa): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/edita-alternativa?objectId=" + alternativa.objectId, alternativa);
    }


}