import { Avaliacao } from './../../model/avaliacao';
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class AvaliacaoService {

    baseDir: string = "http://localhost:1337/parse";
    constructor(private http: Http) { }

    salvarAvaliacao(avaliacao: Avaliacao): Observable<Response> {
        return this.http.post(this.baseDir + "/functions/salvar-avaliacao", avaliacao);
    }
}