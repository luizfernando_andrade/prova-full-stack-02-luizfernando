import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ColetaModel } from '../../model/coleta';

@Injectable()
export class ColetaService {

    //http://localhost
    baseDir: string = 'http://localhost:8080/coleta';
    
    constructor (private http: Http) {}

    salvarColeta(coleta: ColetaModel) {
        this.http.post(this.baseDir + '/salvar-coleta', coleta)
        .subscribe();
    }

    getColetas(): Observable<ColetaModel[]> {
        return this.http.get(this.baseDir + '/carregar-coletas')        
        .map(r =>  r.json())
        .catch(err => err.message);
    }

}