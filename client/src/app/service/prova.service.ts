import { Prova } from './../../model/prova';
import { Item } from './../../model/item';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from "@angular/http";

@Injectable()
export class ProvaService {

    baseDir: string = "http://localhost:1337/parse";
    constructor(private http: Http) { }

    carregarProvas() : Observable<Response> {
        return this.http.post(this.baseDir + "/functions/carregar-provas", {});
    }

    salvarProva(prova: Prova): Observable<Response> {
        return this.http.post(this.baseDir + "/classes/Prova", prova);
    }

    removerProva(prova: Prova): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/deleta-prova?objectId=" + prova.objectId,{});
    }

    editarProva(prova: Prova): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/edita-prova?objectId=" + prova.objectId, prova);
    }
}