import { Item } from './../../model/item';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from "@angular/http";

@Injectable()
export class ItemService {

    baseDir: string = "http://localhost:1337/parse";
    item: Item[] = [];
    constructor(private http: Http) { }

    carregarItens() : Observable<Response> {
        return this.http.post(this.baseDir + "/functions/carregar-itens", {});
    }

    salvarItem(item: Item): Observable<Response> {
        return this.http.post(this.baseDir + "/classes/Item", item);
    }

    removerItem(item: Item): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/deleta-item?objectId=" + item.objectId,{});
    }

    editarItem(item: Item): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/edita-item?objectId=" + item.objectId, item);
    }
}