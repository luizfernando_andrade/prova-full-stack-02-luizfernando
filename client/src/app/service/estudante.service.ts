import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Estudante } from "../../model/estudante";
import { Subscriber } from "rxjs/Subscriber";

@Injectable()
export class EstudanteService {

    baseDir: string = "http://localhost:1337/parse";
    estudantes: Estudante[] = [];
    constructor(private http: Http) { }

    //http://localhost:1337/parse

    carregarEstudantes() : Observable<Response> {
        return this.http.post(this.baseDir + "/functions/carregar-estudantes", {});
    }

    salvarEstudante(estudante: Estudante): Observable<Response> {
        return this.http.post(this.baseDir + "/classes/Estudante", estudante);
    }

    removerEstudante(estudante: Estudante): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/deleta-estudante?objectId=" + estudante.objectId,{});
    }

    editarEstudante(estudante: Estudante): Observable<Response>{
        return this.http.post(this.baseDir + "/functions/edita-estudante?objectId=" + estudante.objectId, estudante);
    }


}