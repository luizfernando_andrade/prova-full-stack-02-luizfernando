import { RespostaEstudante } from './../../model/resposta.estudante';
import { Avaliacao } from './../../model/avaliacao';
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class RespostaService {

    baseDir: string = "http://localhost:1337/parse";
    constructor(private http: Http) { }

    salvarResposta(respostaEstudante: RespostaEstudante): Observable<Response> {
        return this.http.post(this.baseDir + "/classes/RespostaEstudante", respostaEstudante);
    }
}