import { ProvaService } from './service/prova.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing';
import { HttpModule, XHRBackend } from '@angular/http';
import { EstudanteService } from './service/estudante.service';
import { ExtendedXHRBackend } from './core/extended-xhrbackend';
import { FormsModule } from '@angular/forms';
import { AlternativaService } from './service/alternativa.service';
import { EstudanteComponent } from './componentes/estudante/estudante.component';
import { AlternativaComponent } from './componentes/alternativa/alternativa.component';
import { ItemComponent } from './componentes/item/item.component';
import { ItemService } from './service/item.service';
import { ProvaComponent } from './componentes/prova/prova.component';
import { AvaliacaoComponent } from './componentes/avaliacao/avaliacao.component';
import { AvaliacaoService } from './service/avaliacao.service';
import { RespostaService } from './service/resposta.service';
import { ColetaComponent } from './componentes/coleta/coleta.component';
import { ColetaService } from './service/coleta.service';


@NgModule({
  declarations: [
    AppComponent,
    EstudanteComponent,
    AlternativaComponent,
    ItemComponent,
    ProvaComponent,
    AvaliacaoComponent,
    ColetaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [EstudanteService,
              AlternativaService,
              ItemService,
              ProvaService,
              AvaliacaoService,
              RespostaService,
              ColetaService,
              { provide: XHRBackend, useClass: ExtendedXHRBackend }],
  bootstrap: [AppComponent]
})
export class AppModule { }
