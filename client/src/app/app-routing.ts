import { AvaliacaoComponent } from './componentes/avaliacao/avaliacao.component';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { EstudanteComponent } from './componentes/estudante/estudante.component';
import { AlternativaComponent } from './componentes/alternativa/alternativa.component';
import { ItemComponent } from './componentes/item/item.component';
import { ProvaComponent } from './componentes/prova/prova.component';
import { ColetaComponent } from './componentes/coleta/coleta.component';


export const routerConfig: Routes = [
  { path: '', component: AppComponent, children:[{ path: 'estudante', component: EstudanteComponent},
                                                 { path: 'alternativa', component: AlternativaComponent},
                                                 { path: 'item', component: ItemComponent},
                                                 { path: 'prova', component: ProvaComponent},
                                                 { path: 'avaliacao', component: AvaliacaoComponent},
                                                 {path: 'coletas', component: ColetaComponent}] }
  
]

@NgModule({
  imports: [RouterModule.forRoot(routerConfig)],
  exports: [RouterModule]
})
export class AppRoutingModule { }