import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Request, XHRBackend, BrowserXhr, ResponseOptions, XSRFStrategy, Response } from '@angular/http';

@Injectable()
export class ExtendedXHRBackend extends XHRBackend {

    constructor(browserXhr: BrowserXhr, baseResponseOptions: ResponseOptions, xsrfStrategy: XSRFStrategy) {
        super(browserXhr, baseResponseOptions, xsrfStrategy);
    }

    createConnection(request: Request) {

        let xhrConnection = super.createConnection(request);
        request.headers.set('Content-Type', 'application/json');

        return xhrConnection;
    }

}