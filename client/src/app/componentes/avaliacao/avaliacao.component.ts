import { AvaliacaoService } from './../../service/avaliacao.service';
import { RespostaEstudante } from './../../../model/resposta.estudante';
import { Prova } from './../../../model/prova';
import { Component, OnInit } from '@angular/core';
import { EstudanteService } from '../../service/estudante.service';
import { ProvaService } from '../../service/prova.service';
import { Estudante } from '../../../model/estudante';
import { Avaliacao } from '../../../model/avaliacao';
import { Router } from '@angular/router';
import { RespostaService } from '../../service/resposta.service';

@Component({
  selector: 'app-avaliacao',
  templateUrl: './avaliacao.component.html',
  styleUrls: ['./avaliacao.component.scss']
})
export class AvaliacaoComponent implements OnInit {

  provas : Prova[] = [];
  estudantes: Estudante[] = [];
  provaId: string;
  estudanteId: string;
  respostasEstudante: RespostaEstudante[] = [];
  
  idResposta: string;
  idAlternativaCorreta: string;

  avaliacao: Avaliacao = new Avaliacao();

  constructor(private estudanteService: EstudanteService, 
    private provaService: ProvaService, 
    private avaliacaoService: AvaliacaoService, 
    private router: Router,
    private respostaService: RespostaService) { }

  ngOnInit() {
    this.provaService.carregarProvas().subscribe(r => {
      this.provas = r.json().result; 
    });
    this.estudanteService.carregarEstudantes().subscribe(r => {
      this.estudantes = r.json().result;
    });
  }

  setRespostaEstudante(respostaCorretaId: string, idItem: string){
    let resposta: RespostaEstudante = new RespostaEstudante();
    resposta.idAlternativaCorreta = respostaCorretaId;
    resposta.idItem = idItem;
    resposta.idResposta = this.idResposta;
    this.respostasEstudante.push(resposta);
  }

  setAvaliacao() {
    this.provas.forEach(prova => {
      if (prova.objectId === this.provaId) {
        this.avaliacao.prova = prova;
      }
    })
  }

  setEstudante() {
    this.estudantes.forEach(est => {
      if (est.objectId === this.estudanteId) {
        this.avaliacao.estudante = est;
      }
    })
  }

  salvarAvaliacao() {
    this.provas.forEach(prova => {
      if (prova.objectId===this.provaId){
        this.avaliacao.prova = prova;
      }
    });

    this.estudantes.forEach(est => {
      if (est.objectId === this.estudanteId) {
        this.avaliacao.estudante = est;
      }
    });
    
    this.avaliacao.respostasEstudante = this.respostasEstudante;
    console.log(JSON.stringify(this.avaliacao));
      
    this.avaliacaoService.salvarAvaliacao(this.avaliacao).subscribe(r =>{
        
      alert(this.avaliacao)
        
    }, err => {
      alert(err)
    }
  );
  }

}
