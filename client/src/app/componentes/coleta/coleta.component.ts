import { ColetaModel } from './../../../model/coleta';
import { Component, OnInit } from '@angular/core';
import { ColetaService } from '../../service/coleta.service';
import { StatusColeta } from '../../enum/status-coleta';
import { Http } from '@angular/http';

@Component({
  selector: 'app-coleta',
  templateUrl: './coleta.component.html',
  styleUrls: ['./coleta.component.css']
})
export class ColetaComponent implements OnInit {

  public coletasAgendadas: ColetaModel[];
  public statusColeta: StatusColeta;

  constructor(private coletaService: ColetaService, private http: Http) { }

  ngOnInit() {
    this.coletaService.getColetas().subscribe(coletas =>{
      this.coletasAgendadas = coletas;
    })
  }

  editarColeta(coleta: ColetaModel){}

}
