import { ItemService } from './../../service/item.service';
import { AlternativaService } from './../../service/alternativa.service';
import { Alternativa } from './../../../model/alternativa';
import { Component, OnInit } from '@angular/core';
import { Item } from '../../../model/item';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  editar: boolean = false;
  item: Item = new Item();
  itens: Item[] = [];
  alternativas: Alternativa[] = [];
  bufferAlternativas: string[]=[];
  bufferAlternativasDesselecionadas: string[] = [];
  alternativasSelecionadas: Alternativa[] = [];

  constructor(private alternativaService: AlternativaService, private itemService: ItemService) { }

  ngOnInit() {

    this.itemService.carregarItens().subscribe(r => {
      this.itens = r.json().result;
      this.alternativaCorreta(this.itens);

    })
  }

  carregarAlternativas() {
    this.alternativaService.carregarAlternativas().subscribe(r => {
      this.alternativas = r.json().result;
        
      if (this.alternativasSelecionadas.length > 0) {
          this.alternativasSelecionadas.forEach(alt => {
            this.alternativas.forEach((a,i)=>{
              if (alt.codAlternativa === a.codAlternativa){
                this.alternativas.splice(i,1);
              }
            })
          })  
      }
    });
  }

  alternativaCorreta(itens: Item[]){
    itens.forEach(item => {
      item.alternativas.forEach(alternativa =>{
        if(item.alternativaCorretaId === alternativa.objectId){
          item.alternativaCorreta = alternativa;
        }
      })
    })
  }

  editarItem(item: Item) {
    this.carregarAlternativas();
    
    this.editar = true;
    this.item = item;
    this.alternativasSelecionadas = this.item.alternativas;

  }


  removerItem(item: Item) {
    this.itemService.removerItem(item).subscribe(r => {
      this.itens.forEach((value,index) => {
        if (value.objectId === item.objectId){
          this.itens.splice(index, 1);
        }
      });
    })
  }

  cadastrarItem() {
    this.carregarAlternativas();
    this.editar = true;
    this.item = new Item();
  }

  fecharCadastro() {
    this.editar = false;
  }

  salvarItem() {
    this.item.alternativas = this.alternativasSelecionadas;

    if (this.item.objectId && this.item.objectId !== '') {
      this.itemService.editarItem(this.item).subscribe(r => {
        this.editar = false;
      }, err => alert(err));
    } else {
      this.itemService.salvarItem(this.item).subscribe(r => {
        this.editar = false;
        this.item.objectId = r.json().objectId;
        this.itens.push(this.item);
        this.alternativaCorreta(this.itens);
      },
      err => alert(err));
    }
  }

  selecionarAlternativas(codAlternativa: string){
    if (this.bufferAlternativas.indexOf(codAlternativa) > -1){
      this.bufferAlternativas.splice(this.bufferAlternativas.indexOf(codAlternativa), 1);
    } else {
      this.bufferAlternativas.push(codAlternativa)
    }
  }

  desselecionarAlternativas(codAlternativa: string){
    if (this.bufferAlternativasDesselecionadas.indexOf(codAlternativa) > -1){
      this.bufferAlternativasDesselecionadas.splice(this.bufferAlternativasDesselecionadas.indexOf(codAlternativa), 1);
    } else {
      this.bufferAlternativasDesselecionadas.push(codAlternativa)
    }
  }

  adicionarAlternativas() {
    this.bufferAlternativas.forEach(bff =>{
      this.alternativas.forEach((alternativa, index) => {
        if (alternativa.codAlternativa === bff) {
          this.alternativasSelecionadas.push(alternativa);
          this.alternativas.splice(index,1);  
        }
      })
    });
  }

  removerAlternativas() {
    this.bufferAlternativasDesselecionadas.forEach(bff =>{
      this.alternativasSelecionadas.forEach((alternativa, index) => {
        if (alternativa.codAlternativa === bff) {
          this.alternativas.push(alternativa);
          this.alternativasSelecionadas.splice(index,1);  
        }
      })
    });
  }

}
