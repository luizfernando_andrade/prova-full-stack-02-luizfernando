import { ProvaService } from './../../service/prova.service';
import { Item } from './../../../model/item';
import { Component, OnInit } from '@angular/core';
import { Prova } from '../../../model/prova';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ItemService } from '../../service/item.service';

@Component({
  selector: 'app-prova',
  templateUrl: './prova.component.html',
  styleUrls: ['./prova.component.scss']
})
export class ProvaComponent implements OnInit {

  prova: Prova = new Prova();
  itens: Item[] = [];
  itensSelecionados: Item[] = [];
  editar: boolean = false;
  provas: Prova[] = [];

  bufferItens: string[]=[];
  bufferItensDesselecionados: string[] = [];
  
  constructor(private itemService: ItemService, private provaService: ProvaService) { }

  ngOnInit() {

    this.provaService.carregarProvas().subscribe(r => {
      this.provas = r.json().result; 
    });

    this.itemService.carregarItens().subscribe(r => {
      this.itens = r.json().result;
    });
  }

  cadastrarProva() {
    this.carregarItens();
    this.editar = true;  
    this.prova = new Prova();
  }

  editarProva(prova: Prova) {
    this.carregarItens();
    this.editar = true;
    this.prova = prova;
    this.itensSelecionados = prova.itens;
  }

  removerProva(prova: Prova) {
    this.provaService.removerProva(prova).subscribe(r => {
      this.provas.forEach((value,index) => {
        if (value.objectId === prova.objectId){
          this.provas.splice(index, 1);
        }
      });
    })
  }

  carregarItens() {
    this.itemService.carregarItens().subscribe(r => {
      this.itens = r.json().result;
        
      if (this.itensSelecionados.length > 0) {
          this.itensSelecionados.forEach(alt => {
            this.itens.forEach((a,i)=>{
              if (alt.codItem === a.codItem){
                this.itens.splice(i,1);
              }
            })
          })  
      }
    });
  }

  salvarProva() {
    this.prova.itens = this.itensSelecionados;
    if (this.prova.objectId && this.prova.objectId !== '') {
      this.provaService.editarProva(this.prova).subscribe(r => {
        this.editar = false;
      },
      err => alert(err));
    } else {
      this.provaService.salvarProva(this.prova).subscribe(r =>{
        this.prova.objectId = r.json().objectId;
        this.provas.push(this.prova);
        this.editar = false;
      },
      err => alert(err));
    }
     
  }

  fecharCadastro() {
    this.editar = false;
  }


  selecionarItens(codItem: string){
    if (this.bufferItens.indexOf(codItem) > -1){
      this.bufferItens.splice(this.bufferItens.indexOf(codItem), 1);
    } else {
      this.bufferItens.push(codItem)
    }
  }

  desselecionarItens(codItem: string){
    if (this.bufferItensDesselecionados.indexOf(codItem) > -1){
      this.bufferItensDesselecionados.splice(this.bufferItensDesselecionados.indexOf(codItem), 1);
    } else {
      this.bufferItensDesselecionados.push(codItem)
    }
  }

  adicionarItens() {
    this.bufferItens.forEach(bff =>{
      this.itens.forEach((item, index) => {
        if (item.codItem === bff) {
          this.itensSelecionados.push(item);
          this.itens.splice(index,1);  
        }
      })
    });
  }

  removerItens() {
    this.bufferItensDesselecionados.forEach(bff =>{
      this.itensSelecionados.forEach((item, index) => {
        if (item.codItem === bff) {
          this.itens.push(item);
          this.itensSelecionados.splice(index,1);  
        }
      })
    });
  }

}
