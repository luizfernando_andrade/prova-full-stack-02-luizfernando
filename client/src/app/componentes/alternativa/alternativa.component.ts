
import { Component, OnInit } from '@angular/core';
import { Alternativa } from '../../../model/alternativa';
import { AlternativaService } from '../../service/alternativa.service';

@Component({
  selector: 'app-alternativa',
  templateUrl: './alternativa.component.html',
  styleUrls: ['./alternativa.component.scss']
})
export class AlternativaComponent implements OnInit {
  
  editar: boolean = false;
  alternativa: Alternativa = new Alternativa();
  alternativas: Alternativa[] = [];
  constructor(private alternativaService: AlternativaService) { }

  ngOnInit() {
    this.alternativaService.carregarAlternativas().subscribe(r => {
      this.alternativas = r.json().result;
    });
  }

  fecharCadastro(){
    this.editar = !this.editar
  }

  cadastrarAlternativa(){
    this.editar = true;
    this.alternativa = new Alternativa();
  }

  removerAlternativa(alternativa: Alternativa) {
    this.alternativaService.removerAlternativa(alternativa).subscribe(est => {
      debugger
      this.alternativas.forEach((value,index) => {
        if (value.objectId === alternativa.objectId){
          this.alternativas.splice(index, 1);
        }
      });
    })
  }

  editaAlternativa(alternativa: Alternativa) {
    this.editar = true;
    this.alternativa = alternativa;
  }

  salvarAlternativa(){
    debugger
    if (this.alternativa.objectId && this.alternativa.objectId !== '') {
      this.alternativaService.editarAlternativa(this.alternativa).subscribe(suc => {
        alert(suc);
      }, err =>{
          alert(err) ;
      });
      this.fecharCadastro();
    } else {
      this.alternativaService.salvarAlternativa(this.alternativa).subscribe(alt => {
        debugger
        this.alternativa.objectId = alt.json().objectId;
        this.alternativas.push(this.alternativa);
        this.alternativa = new Alternativa();
        this.fecharCadastro();
      },
      err => alert(err));
    }
  }
}
