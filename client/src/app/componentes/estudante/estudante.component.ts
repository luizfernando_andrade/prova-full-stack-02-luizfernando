
import { Component, OnInit } from '@angular/core';
import { Estudante } from '../../../model/estudante';
import { EstudanteService } from '../../service/estudante.service';

@Component({
  selector: 'app-estudante',
  templateUrl: './estudante.component.html',
  styleUrls: ['./estudante.component.scss']
})
export class EstudanteComponent implements OnInit {

  estudantes: Estudante[] = [];
  editar: boolean = false;
  estudante: Estudante = new Estudante();

  constructor(private estudanteService: EstudanteService) { }

  ngOnInit() {
    this.estudanteService.carregarEstudantes().subscribe(r => {
      this.estudantes = r.json().result;
    });
  }

  editaEstudante(estudante: Estudante){
    this.estudante = estudante;
    this.editar = !this.editar
  }

  fecharCadastro(){
    this.editar = !this.editar
  }

  removerEstudante(estudante: Estudante) {
    this.estudanteService.removerEstudante(estudante).subscribe(est => {
      this.estudantes.forEach((value,index) => {
        if (value.objectId === estudante.objectId){
          this.estudantes.splice(index, 1);
        }
      });
    })
  }

  salvarEstudante() {
    if (this.estudante.objectId && this.estudante.objectId !== '') {
      this.estudanteService.editarEstudante(this.estudante).subscribe(suc => alert(suc), err => alert(err));
      this.fecharCadastro();
    } else {
      this.estudanteService.salvarEstudante(this.estudante).subscribe(est => {
        this.estudante.objectId = est.json().objectId;
        this.estudantes.push(this.estudante);
        this.estudante = new Estudante();
        this.fecharCadastro();
      },
      err => alert(err));
    }
  }

  cadastrarEstudante() {
    this.editar = true;
    this.estudante = new Estudante();
  }

}
