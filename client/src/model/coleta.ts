import { InstituicaoModel } from './instituicao';
import { Usuario } from './usuario';
import { TipoReciclavel } from '../app/enum/tipo-reciclavel';
import { StatusColeta } from '../app/enum/status-coleta';
export class ColetaModel {
    
    usuario: Usuario;
    data: Date;
    instituicao: InstituicaoModel;
    volume: number;
    tipoReciclavel: TipoReciclavel;
    status?: StatusColeta;
}