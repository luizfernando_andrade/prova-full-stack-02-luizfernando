import { Estudante } from './estudante';
import { Prova } from './prova';
import { RespostaEstudante } from './resposta.estudante';
export class Avaliacao {
    
    objectId?: string;
    estudante?: Estudante;
    prova?: Prova;
    respostasEstudante?: RespostaEstudante[]; 
}