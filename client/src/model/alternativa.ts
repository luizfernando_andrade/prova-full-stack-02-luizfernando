import { TipoAlternativa } from "../app/enum/tipo.alternativa";

export class Alternativa {
    objectId?:string;
    codAlternativa?: string;
    tipoAlternativa?: TipoAlternativa;
    texto?:string;
    imagem?:File;
}