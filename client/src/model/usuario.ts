import { TipoUsuario } from "../app/enum/tipo-usuario";

export class Usuario {
    id?: number;
    nome: string = '';
    email: string = '';
    tipo: TipoUsuario;
    senha: string = '';
    telefone: string = '';

}