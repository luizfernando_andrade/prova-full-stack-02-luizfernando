export class InstituicaoModel {

    id?: number;
    nome: string;
    descricao: string;
    icone: string;

}