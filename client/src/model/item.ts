import { Alternativa } from "./alternativa";

export class Item {

    objectId?: string;
    codItem?: string;
    nome?: string;
    tipoRenderizacao?: string;
    obrigatorio?: boolean;
    texto?: string;
    alternativas?: Alternativa[];
    alternativaCorretaId?: string;
    alternativaCorreta: Alternativa;

}