function apenasLetras(valor) {
    var regex = new RegExp("^[a-zA-Z]+$");
    return regex.test(valor);
}
function checarTamanho(valor, tamanhoMaximo) {
    return valor.length <= tamanhoMaximo;
}

function validarCampoVazio(valor){
    return valor && valor != '';
}

function emailvalido(email) {
    var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return reg.test(email);
}

function validarAlfaNumerico(valor) {
    var reg = /^([a-zA-Z0-9_-]+)$/
    return reg.test(valor);
}

module.exports = {
    validarAlfaNumerico: validarAlfaNumerico,
    emailvalido: emailvalido,
    apenasLetras: apenasLetras,
    checarTamanho: checarTamanho,
    validarCampoVazio: validarCampoVazio
}


