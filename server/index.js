var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
var mongodb = require('mongodb');

var databaseUri ='mongodb://localhost:27017/caed';

if (!databaseUri) {
  console.log('Banco de dados não especificado.');
}

var api = new ParseServer({
  databaseURI: databaseUri,
  cloud: __dirname + '/cloud/main.js',
  appId: 'caed-app',
  masterKey: 'master',
  serverURL: 'http://localhost:1337/parse', 
  liveQuery: {
    classNames: ["Posts", "Comments"] 
  }
});


var app = express();


app.use('/public', express.static(path.join(__dirname, '/public')));


var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

var port = 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('parse-server disponível na porta: ' + port + '.');
});

ParseServer.createLiveQueryServer(httpServer);
