var validador = require("../../validador/validador.utils.js");
Parse.Cloud.define('carregar-alternativas', function(req, res) {
    
    var Alternativa = Parse.Object.extend("Alternativa");
    var query =  new Parse.Query(Alternativa);

    query.find().then(function(values) {
        res.success(values);
      },
      function(err){
        res.error(err);
      });

});

Parse.Cloud.define('edita-alternativa', function(req, res) {
    
    var Alternativa = Parse.Object.extend("Alternativa");
    var alternativa = new Alternativa();
    alternativa.set("objectId", req.params.objectId);
    alternativa.set("codAlternativa", req.params.codAlternativa);
    alternativa.set("tipoAlternativa", req.params.tipoAlternativa);
    alternativa.set("texto", req.params.texto);
    
    
    alternativa.save(null, {
        success: function(){
            res.success(estudante);
        }, 
        error: function(error) {
            res.error(error)
        }
    });
});

Parse.Cloud.define('deleta-alternativa', function(req, res) {

    var Alternativa = Parse.Object.extend("Alternativa");
    var alternativa = new Alternativa();
    
    alternativa.set("objectId", req.params.objectId);
    
    alternativa.destroy({
        success: function(){
            res.success(alternativa);
        }, 
        error: function(error) {
            res.error(error)
        }
    });
    
});