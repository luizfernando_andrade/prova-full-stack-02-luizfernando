
Parse.Cloud.define('edita-prova', function(req, res) {
    
    var Prova = Parse.Object.extend("Prova");
    var prova = new Prova();

    prova.set("objectId", req.params.objectId);
    prova.set("nome", req.params.nome);
    prova.set("dataInicio", req.params.dataInicio);
    prova.set("dataFim", req.params.dataFim);
    prova.set("itens", req.params.itens);


    prova.save(null, {
        success: function(){
            res.success(estudante);
        }, 
        error: function(error) {
            res.error(error)
        }
    });
});

Parse.Cloud.define('carregar-provas', function(req, res) {
    
    var Prova = Parse.Object.extend("Prova");
    var query =  new Parse.Query(Prova);

    query.find().then(function(values) {
        res.success(values);
      },
      function(err){
        res.error(err);
      });

});

Parse.Cloud.define('deleta-prova', function(req, res) {

var Prova = Parse.Object.extend("Prova");
var prova = new Prova();

prova.set("objectId", req.params.objectId);

prova.destroy({
    success: function(){
        res.success(prova);
    }, 
    error: function(error) {
        res.error(error)
    }
});

});