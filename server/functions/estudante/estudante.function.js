var validador = require("../../validador/validador.utils.js");

Parse.Cloud.define('edita-estudante', function(req, res) {
    
    var Estudante = Parse.Object.extend("Estudante");
    var estudante = new Estudante();

    estudante.set("objectId", req.params.objectId);
    estudante.set("nome", req.params.nome);
    estudante.set("sobrenome", req.params.sobrenome);
    estudante.set("email", req.params.email);
    
    if (!validador.apenasLetras(req.params.nome) || !validador.apenasLetras(req.params.sobrenome)) {
        res.error({"Erro message": "O nome ou sobrenome do estudante deve conter apenas letras"});
    } else if(!validador.emailvalido(req.params.email)){
        res.error({"Erro message": "Deve ser inserido um email válido"});
    } else if (!validador.checarTamanho(req.params.nome, 100) || !validador.checarTamanho(req.params.sobrenome, 100)) {
        res.error({"Erro message": "O nome ou sobrenome devem ser menores que 100 caracteres cada"});
    }  else {
        estudante.save(null, {
            success: function(){
                res.success(estudante);
            }, 
            error: function(error) {
                res.error(error)
            }
        });
    }


});

Parse.Cloud.define('carregar-estudantes', function(req, res) {
    
    var Estudante = Parse.Object.extend("Estudante");
    var query =  new Parse.Query(Estudante);

    query.find().then(function(values) {
        res.success(values);
      },
      function(err){
        res.error(err);
      });

});

Parse.Cloud.define('deleta-estudante', function(req, res) {

var Estudante = Parse.Object.extend("Estudante");
var estudante = new Estudante();

estudante.set("objectId", req.params.objectId);

estudante.destroy({
    success: function(){
        res.success(estudante);
    }, 
    error: function(error) {
        res.error(error)
    }
});

});