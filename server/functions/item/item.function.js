
Parse.Cloud.define('edita-item', function(req, res) {
    
    var Item = Parse.Object.extend("Item");
    var item = new Item();

    item.set("objectId", req.params.objectId);
    item.set("codItem", req.params.codItem);
    item.set("nome", req.params.nome);
    item.set("tipoRenderizacao", req.params.tipoRenderizacao);
    item.set("obrigatorio", req.params.obrigatorio);
    item.set("alternativas", req.params.alternativas);
    item.set("alternativaCorretaId", req.params.alternativaCorretaId);
    

    item.save(null, {
        success: function(){
            res.success(estudante);
        }, 
        error: function(error) {
            res.error(error)
        }
    });
});

Parse.Cloud.define('carregar-itens', function(req, res) {
    
    var Item = Parse.Object.extend("Item");
    var query =  new Parse.Query(Item);

    query.find().then(function(values) {
        res.success(values);
      },
      function(err){
        res.error(err);
      });

});

Parse.Cloud.define('deleta-item', function(req, res) {

var Item = Parse.Object.extend("Item");
var item = new Item();

item.set("objectId", req.params.objectId);

item.destroy({
    success: function(){
        res.success(item);
    }, 
    error: function(error) {
        res.error(error)
    }
});

});